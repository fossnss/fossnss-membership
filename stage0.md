# How to get started?

### There are 3 Stages for joining __FOSSNSS__. 

This includes simple tasks and can be completed by anyone having access to the internet and a computer.

Instructions have been supplied below, ensuring a complete beginner can finish the task.

We recommend using any Linux based OS as the primary environment for completing the tasks.

_If you feel stuck at any stage, or need special arrangement, ping us at the Telegram group/ email us at foss@nssce.ac.in_

------
## Stage 0: 

### **1. Create an account in GitHub**

GitHub is a code hosting platform for version control and collaboration. It lets you and others work together on projects from anywhere.

**Create your account here: https://github.com/login**

The projects on GitHub are examples of open-source software. There are millions of open source projects on GitHub.There are a variety of ways that you can contribute to open source projects. You can search among the millions of project repositories available on GitHub and contribute as you wish!

**Take a look at our official page : https://github.com/fossnss**
 
### **2. Create your first repository**

A GitHub repository can be used to store a development project. It can contain folders and any type of files (HTML, CSS, JavaScript, Documents, Data, Images). 

What if you create a repository that presents yourself? This will be your next task. 

GitHub has many special repositories. You can create a repository that matches your username, add a README file to it, and all the information in that file will be visible on your GitHub profile. 

  **a) Create a new repository, you must use your username as the repository name**

  **b) Initialize the repo with a README.md**

  ![Creating the special repository](images/create-repo.png)
 
  **c) Write about yourself in the README.md file.**
  
  _Introduce yourself, write about the skills you possess, write about your interests!_

  **d) Create it with the “Create repository” button**

  **e) Now you can view your README.md on your GitHub profile**

 _You can customize your new Identity repository as you wish!  (You can use Markdown to make it look better!)_

  **f) Take a screenshot of your GitHub profile**

  ![Profile with readme](images/profile-with-readme.png)


 To verify completion of the aforementioned, **capture a screenshot** .  You can submit the screenshot in next task.




