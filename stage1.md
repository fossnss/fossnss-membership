# Stage 1: 


### 1. Create an account in GitLab

GitLab is a web-based DevOps lifecycle tool that provides a Git repository manager providing wiki, issue-tracking and continuous integration and deployment pipeline features, using an open-source license, developed by GitLab Inc.

**Create your account here: https://gitlab.com/users/sign_in**

Our hosting, CI pipelines are supported by GitLab with licensing us with their golden edition. All our projects are hosted on GitLab, since it aligns more with free and open source ideology rather than GitHub. Our members will benefit from having seats in the Ultimate SaaS Plan from GitLab for projects.


### 2. Fork and star our repository

Make sure you forked and starred our repository at **https://gitlab.com/fossnss/website**. This will help us to verify your GitLab account.

![Fork and star](images/gitlab-image.jpg)

### 3. Run the site locally

_Follow the steps below to run the website locally, the commands below are meant to be run in a Terminal emulator:_

   1. #### Clone the repository

   Now you need to clone this repo using git to your local machine to develop or to test.
   
   \# if you have not added ssh keys to your gitlab account:

   ```git clone https://gitlab.com/fossnss/website.git fossnss```

   \# else, if you have ssh added your ssh keys:
         
   ```git clone git@gitlab.com:fossnss/website.git fossnss```

   2. #### Install NodeJs and NPM

   We recommend using NodeJs version 14(LTS). To manage different versions of NodeJS installations, you may use nvm script for bash or your favourite shell. Follow [this official guide](https://github.com/nvm-sh/nvm) to add nvm to your shell

   ```
   # install NodeJs v14 and use it in nvm
   nvm install --lts
   nvm use 14
   ```

   3. #### Install GatsbyJS

   Now you need to install GatsbyJS, which is used to generate our static site. Follow [this official guide](https://www.gatsbyjs.org/tutorial/part-zero/#using-the-gatsby-cli) to install GatsbyJS onto your machine.

   ```
   npm install
   npm install -g gatsby-cli
   ```

   If there occurs any kind of `warning` check [here](https://stackoverflow.com/a/52378998) 

   4. #### Run website locally

   `gatsby develop`

   Your site is now running at http://localhost:8000!

   _Note: You'll also see a second link: `http://localhost:8000/___graphql.`This is a tool you can use to experiment with querying your data. Learn more about using this tool in the [Gatsby tutorial](https://www.gatsbyjs.com/docs/tutorial/part-5/#introducing-graphiql)._
   
   5. #### Take the screenshot of the site, running locally

   To verify completion of the aforementioned, **capture a screenshot** in the given format (make sure the site is running locally). Check the [screenshot template here](https://gitlab.com/fossnss/fossnss-membership/-/blob/master/images/screen-shot.png). You can submit the screenshot in next task.


### 4. Create a new **membership-submission** issue

_[Final Step for the completion of Stage 0]_

1. **Create a new issue at: https://gitlab.com/fossnss/fossnss-membership/-/issues/new**

2. Select **Membership Request** from **Choose a template** in **Description**. This will create a form inside the issue box. Fill-in the details.

![Choosing Membership Request template](images/Membership-request.png)

3. **'Attach a file'** option can be found at bottom of text-box for **submitting the screenshots** from the previous tasks. **This is mandatory for the completion of tasks**

4. Set the issue title as **"Membership Submission"**

5. Mark the checkbox **"This issue is confidential and should only be visible to team members with at least Reporter access."**

6. Set the **Labels** option as "membership-submission".

7. Complete the submission by selecting **Create issue**

<br>
<br>
<br>

#### Kaboom! You have successfully completed Stage 0 and Stage 1 of the FOSSNSS Membership drive. You have run our website locally, and learned how to start a new issue and a new repository. These are some basic stuff about open source. To take it further, try to use Linux as primary OS, and start learning Git - the most popular version control system.
<br>
<br>
