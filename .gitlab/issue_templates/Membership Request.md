---
Instructions-
1. Fill in the form below
2. 'Attach a file' option can be found below for submitting the screenshot from previous task
3. Set the title as "Membership Submission"
4. Mark the checkbox "This issue is confidential and should only be visible to team members with at least Reporter access."
---

## Stage 0: Membership Submission
---
<br>
<br>


- Full Name: 

- Branch and Year:

- E-mail ID:

- Contact Number:

- Interests:

    (eg: Web, Design, Documentation or anything you like to learn)

<br>

- Which OS you use? (_Linux-based or Other_):



<br>
<br>

- Why you want to be in our team, and what do you expect being with us? (_keep it as short and specific as you can_):




<br>

---
- ##### Other comments or queries:


<br>
<br>

---
<!-- Don't forget to make this issue CONFIDENTIAL -->
<!-- To see the this in formatted version, click Preview option at the top of box -->